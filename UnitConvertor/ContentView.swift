//
//  ContentView.swift
//  UnitConvertor
//
//  Created by Vancea Tapalaga Victor on 25.01.2023.
//

import SwiftUI

struct ContentView: View {
    private let unitsIn = ["temperature","length","time","volume"]
    private let unitsLength = ["meters", "kilometers", "feet", "yards", "miles"]
    private let unitsTemp = ["Celsius", "Fahrenheit","Kelvin"]
    private let unitsTime = ["seconds", "minutes", "hours","days"]
    private let unitsVolume = ["milliliters", "liters", "cups", "pints",  "gallons"]
    @State private var type : String = "temperature"
    @State private var unit : String = ""
    @State private var unit2 : String = ""
    @State private var value : Double = 0
    @FocusState private var valueIsFocused : Bool
    
    
    var tempConvertor : Double  {
        var degrees : Double = value
        var rez : Double = value
        if(unit == "Fahrenheit"){
             degrees = (value - 32)/1.8000
            rez = degrees
        }else if(unit == "Kelvin"){
            degrees = value - 273.15
            rez = degrees
        }
        if unit2 == "Fahrenheit"{
            rez = degrees * 1.8 + 32
        }
        else if unit2 == "Kelvin"{
            rez = degrees + 273.15
        }
        return rez
    }
    
    var lengthConvertor : Double  {
        var distance : Double = value
        var rez : Double = value
        if(unit == "kilometers"){
             distance = value * 1000 // converts to meters always
            rez = distance
        }else if(unit == "feet"){
            distance = value * 0.3048
            rez = distance
        }else if(unit == "yards"){
            distance = value *  0.9144
            rez = distance
        }
        else if(unit == "miles"){
            distance = value * 1609.344
            rez = distance
        }
        if unit2 == "kilometers"{
            rez = distance / 1000
        }
        else if unit2 == "feet"{
            rez = distance / 0.3048
        }
        else if unit2 == "yards"{
            rez = distance / 0.9144
        }
        else if unit2 == "miles"{
            rez = distance / 1609.344
        }
       
        return rez
    }
    var timeConvertor : Double {
        var time : Double = value
        var rez :Double = value
        if (unit == "minutes"){
            time = value * 60 // converts to minutes always
            rez = time
        }else if unit == "hours"{
            time = value * 3600
            rez = time
        }else if unit == "days"{
            time = value * 24 * 60 * 60
            rez = time
        }
        
         if unit2 == "minutes"{
            rez = time/60
        }else if unit2 == "hours"{
            rez = time / 3600
        } else if unit2 == "days"{
            rez = time / (60 * 24 * 60)
        }
        return rez
    }
    var volumeConvertor : Double{
        var vol : Double = value
        var rez : Double = value
        
        if unit == "liters"{
            vol = value * 1000
            rez = vol
        }else if unit == "cups"{
            vol = value * 236.588237
            rez = vol
        }else if unit == "pints"{
            vol = value * 473.176473
            rez = vol
        }else if unit == "gallons"{
            vol = value * 3785
            rez = vol
        }
        
        if unit2 == "liters"{
            rez = vol / 1000
        }else if unit2 == "cups"{
            rez = vol / 236.588237
        }else if unit2 == "pints"{
            rez = vol / 473.176473
        } else if unit2 == "gallons"{
            rez = vol / 3785
        }
        return rez
        
    }
    var body: some View {
        NavigationView{
            Form{
                Section{
                    Picker("Type : ",selection: $type){
                        ForEach(unitsIn,id: \.self){
                            Text("\($0)")
                        }
                    }
                }header: {
                    Text("Choose the type to convert")
                }.pickerStyle(.segmented)
                Section{
                    
                        Picker("Unit 1 : ",selection: $unit){
                            if type == "temperature"{
                               
                                ForEach(unitsTemp,id: \.self){
                                    Text($0)
                                }
                            }else if type == "length"{
                                ForEach(unitsLength,id: \.self){
                                    Text($0)
                                }
                            }else if type == "time"{
                                ForEach(unitsTime,id: \.self){
                                    Text($0)
                            }
                           
                            }else {
                                ForEach(unitsVolume,id: \.self){
                                    Text($0)
                            }
                        }
                    }
                }header: {
                    Text("Select the first unit")
                }
                Section{
                    Picker("Unit 2 : ",selection: $unit2){
                        if type == "temperature"{
                           
                            ForEach(unitsTemp,id: \.self){
                                Text($0)
                            }
                        }else if type == "length"{
                            ForEach(unitsLength,id: \.self){
                                Text($0)
                            }
                        }else if type == "time"{
                        ForEach(unitsTime,id: \.self){
                            Text($0)
                        }
                       
                        }else {
                        ForEach(unitsVolume,id: \.self){
                            Text($0)
                        }
                    }
                }
                }header: {
                    Text("Select the second unit")
                }
                Section{
                    TextField("Value : ",value: $value,format : .number).keyboardType(.decimalPad).focused($valueIsFocused)
                } header: {
                    Text("Value to convert")
                }
                Section{
                    if type == "temperature" {
                        Text("Converted value is : \( tempConvertor.formatted())")
                    }else if type == "length"{
                        Text("Converted value is : \( lengthConvertor.formatted()) ")
                    }else if type == "time"{
                        Text("Converted value is : \(timeConvertor.formatted())")
                    }else if type == "volume"{
                        Text("Converted value is : \(volumeConvertor.formatted())")
                    }
                    
                
                }
                
            }.navigationBarTitle("Unit Convertor App",displayMode: .inline)
                .toolbar{
                    ToolbarItemGroup(placement: .keyboard){
                        // let us specify tool bars items for view , top or bottom as examples
                           // here we place buttons on the toolbar , we specify in the placement that the buttons should be on the keyboard
                                Spacer() // Place the 'Done' button to the right side of the toolBar , depending on what we use
                                Button("Done"){
                                    valueIsFocused = false
                                }
                    }
                }
            
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
