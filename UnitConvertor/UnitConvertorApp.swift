//
//  UnitConvertorApp.swift
//  UnitConvertor
//
//  Created by Vancea Tapalaga Victor on 25.01.2023.
//

import SwiftUI

@main
struct UnitConvertorApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
